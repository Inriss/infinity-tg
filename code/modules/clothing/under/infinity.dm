/obj/item/clothing/under/dress_cap
	name = "captain's dress uniform"
	desc = "Feminine fashion for the style concious captain."
	icon_state = "dress_cap"
	item_state = "lawyer_blue"
	item_color = "dress_cap"
	worn_icon = 'icons/mob/paradise_clothes.dmi'
	icon = 'icons/obj/clothing/paradise.dmi'
	fitted = FEMALE_UNIFORM_TOP
	can_adjust = 0

/obj/item/clothing/under/dress_hop
	name = "head of personnel dress uniform"
	desc = "Feminine fashion for the style concious HoP."
	icon_state = "dress_hop"
	item_state = "lawyer_blue"
	item_color = "dress_hop"
	worn_icon = 'icons/mob/paradise_clothes.dmi'
	icon = 'icons/obj/clothing/paradise.dmi'
	fitted = FEMALE_UNIFORM_TOP
	can_adjust = 0

/obj/item/clothing/under/dress_rd
	name = "research director's dress uniform"
	desc = "Feminine fashion for the style concious RD."
	icon_state = "dress_rd"
	item_state = "brownjsuit"
	item_color = "dress_rd"
	icon = 'icons/obj/clothing/paradise.dmi'
	worn_icon = 'icons/mob/paradise_clothes.dmi'
	fitted = FEMALE_UNIFORM_TOP
	can_adjust = 0
	armor = list(melee = 0, bullet = 0, laser = 0,energy = 0, bomb = 10, bio = 10, rad = 0)

/obj/item/clothing/under/mai_yang
	name = "white cheongsam"
	desc = "It is a white cheongsam dress."
	icon_state = "mai_yang"
	item_state = "w_suit"
	item_color = "mai_yang"
	icon = 'icons/obj/clothing/paradise.dmi'
	worn_icon = 'icons/mob/paradise_clothes.dmi'
	fitted = FEMALE_UNIFORM_TOP
	can_adjust = 0

/obj/item/clothing/under/dress_pink
	name = "pink dress"
	desc = "A simple, tight fitting pink dress."
	icon_state = "dress_pink"
	item_state = "p_suit"
	item_color = "dress_pink"
	icon = 'icons/obj/clothing/paradise.dmi'
	worn_icon = 'icons/mob/paradise_clothes.dmi'
	fitted = FEMALE_UNIFORM_TOP
	can_adjust = 0

/obj/item/clothing/under/dress_green
	name = "green dress"
	desc = "A simple, tight fitting green dress."
	icon_state = "dress_green"
	item_state = "g_suit"
	item_color = "dress_green"
	icon = 'icons/obj/clothing/paradise.dmi'
	worn_icon = 'icons/mob/paradise_clothes.dmi'
	fitted = FEMALE_UNIFORM_TOP
	can_adjust = 0

/obj/item/clothing/under/sundress_white
	name = "white sundress"
	desc = "A white sundress decorated with purple lilies."
	icon_state = "sundress_white"
	item_state = "w_suit"
	item_color = "sundress_white"
	icon = 'icons/obj/clothing/paradise.dmi'
	worn_icon = 'icons/mob/paradise_clothes.dmi'
	fitted = FEMALE_UNIFORM_TOP
	can_adjust = 0

/obj/item/clothing/under/dress_fire
	name = "flame dress"
	desc = "A small black dress with blue flames print on it."
	icon_state = "dress_fire"
	item_state = "bl_suit"
	item_color = "dress_fire"
	icon = 'icons/obj/clothing/paradise.dmi'
	worn_icon = 'icons/mob/paradise_clothes.dmi'
	fitted = FEMALE_UNIFORM_TOP
	can_adjust = 0

/obj/item/clothing/under/dress_yellow
	name = "yellow dress"
	desc = "A fancy yellow gown for those who like to show leg."
	icon_state = "dress_yellow"
	item_state = "y_suit"
	item_color = "dress_yellow"
	icon = 'icons/obj/clothing/paradise.dmi'
	worn_icon = 'icons/mob/paradise_clothes.dmi'
	fitted = FEMALE_UNIFORM_TOP
	can_adjust = 0

/obj/item/clothing/under/warden_corporate
	name = "warden corporate uniform"
	desc = "Unusual warden's uniform."
	icon_state = "warden_corporate"
	item_state = "bl_suit"
	item_color = "warden_corporate"
	icon = 'icons/obj/clothing/paradise.dmi'
	worn_icon = 'icons/mob/paradise_clothes.dmi'
	can_adjust = 0
	armor = list(melee = 10, bullet = 0, laser = 0,energy = 0, bomb = 0, bio = 0, rad = 0)
	alt_covers_chest = 1

/obj/item/clothing/under/sec_corporate
	name = "corporate security uniform"
	desc = "Unusual security uniform."
	icon_state = "sec_corporate"
	item_state = "bl_suit"
	item_color = "sec_corporate"
	icon = 'icons/obj/clothing/paradise.dmi'
	worn_icon = 'icons/mob/paradise_clothes.dmi'
	can_adjust = 0
	armor = list(melee = 10, bullet = 0, laser = 0,energy = 0, bomb = 0, bio = 0, rad = 0)
	alt_covers_chest = 1

/obj/item/clothing/under/hos_corporate
	name = "corporate head of security's uniform"
	desc = "Unusual security uniform."
	icon_state = "hos_corporate"
	item_state = "bl_suit"
	item_color = "hos_corporate"
	icon = 'icons/obj/clothing/paradise.dmi'
	worn_icon = 'icons/mob/paradise_clothes.dmi'
	can_adjust = 0
	armor = list(melee = 10, bullet = 0, laser = 0,energy = 0, bomb = 0, bio = 0, rad = 0)
	alt_covers_chest = 1

/obj/item/clothing/under/hos_formal_female
	name = "head of security's formal uniform"
	desc = "A female head of security's formal-wear, for special occasions."
	icon_state = "hos_formal_female"
	item_state = "r_suit"
	item_color = "hos_formal_female"
	icon = 'icons/obj/clothing/paradise.dmi'
	worn_icon = 'icons/mob/paradise_clothes.dmi'
	can_adjust = 0

/obj/item/clothing/under/dress_saloon
	name = "saloon girl dress"
	desc = "A old western inspired gown for the girl who likes to drink."
	icon_state = "dress_saloon"
	item_state = "r_suit"
	item_color = "dress_saloon"
	worn_icon = 'icons/mob/paradise_clothes.dmi'
	icon = 'icons/obj/clothing/paradise.dmi'
	fitted = FEMALE_UNIFORM_TOP
	can_adjust = 0

/obj/item/clothing/under/ce_female_shorts
	name = "chief engineer's female shorts"
	desc = "A light and stylish CE shorts!"
	icon_state = "ce_female_shorts"
	item_state = "y_suit"
	item_color = "ce_female_shorts"
	icon = 'icons/obj/clothing/infinity_work.dmi'
	worn_icon = 'icons/mob/infinity_work.dmi'
	armor = list(melee = 0, bullet = 0, laser = 0,energy = 0, bomb = 0, bio = 0, rad = 10)
	burn_state = -1

/obj/item/clothing/under/cmo_dress
	name = "chief medical officer's female dress"
	desc = "Feminine fashion for the style concious CMO."
	icon_state = "cmo_dress"
	item_state = "w_suit"
	item_color = "cmo_dress"
	icon = 'icons/obj/clothing/infinity_work.dmi'
	worn_icon = 'icons/mob/infinity_work.dmi'
	fitted = FEMALE_UNIFORM_TOP
	can_adjust = 0
	permeability_coefficient = 0.50
	armor = list(melee = 0, bullet = 0, laser = 0,energy = 0, bomb = 0, bio = 10, rad = 0)

/obj/item/clothing/under/schoolgirlblack
	name = "black schoolgirl uniform"
	desc = "It's just like one of my Japanese animes!"
	icon_state = "schoolgirlblack"
	item_state = "bl_suit"
	item_color = "schoolgirlblack"
	worn_icon = 'icons/mob/infinity_work.dmi'
	icon = 'icons/obj/clothing/infinity_work.dmi'
	can_adjust = 0
	fitted = FEMALE_UNIFORM_TOP

/obj/item/clothing/under/wfleetcommander
	name = "white fleet commander uniform"
	desc = "A good and fashion suit for the commander"
	icon_state = "wfleetcommander"
	item_state = "w_suit"
	item_color = "wfleetcommander"
	worn_icon = 'icons/mob/infinity_work.dmi'
	icon = 'icons/obj/clothing/infinity_work.dmi'
	can_adjust = 0

/obj/item/clothing/under/antiquated_skirt
	name = "antiquated skirt"
	desc = "A some old skirt"
	icon_state = "antiquated_skirt"
	item_state = "bl_suit"
	item_color = "antiquated_skirt"
	worn_icon = 'icons/mob/infinity_work.dmi'
	icon = 'icons/obj/clothing/infinity_work.dmi'
	can_adjust = 0
	fitted = FEMALE_UNIFORM_TOP
	body_parts_covered = GROIN|LEGS|FEET

/obj/item/clothing/under/red_cheongasm
	name = "red cheongasm"
	desc = "It is a red velvet cheongsam dress."
	icon_state = "red_cheongasm"
	item_state = "r_suit"
	item_color = "red_cheongasm"
	worn_icon = 'icons/mob/infinity_work.dmi'
	icon = 'icons/obj/clothing/infinity_work.dmi'
	can_adjust = 0
	fitted = FEMALE_UNIFORM_TOP

/obj/item/clothing/under/kos_shorts
	name = "kos shorts"
	desc = "Small leather shorts!"
	icon_state = "kos_shorts"
	item_state = ""
	item_color = "kos_shorts"
	worn_icon = 'icons/mob/infinity_work.dmi'
	icon = 'icons/obj/clothing/infinity_work.dmi'
	can_adjust = 0

/obj/item/clothing/under/kos_bshorts
	name = "black kos shorts"
	desc = "Small black leather shorts!"
	icon_state = "kos_bshorts"
	item_state = ""
	item_color = "kos_bshorts"
	worn_icon = 'icons/mob/infinity_work.dmi'
	icon = 'icons/obj/clothing/infinity_work.dmi'
	can_adjust = 0

/obj/item/clothing/under/kos_bshorts2
	name = "black kos shorts"
	desc = "Small black leather shorts!"
	icon_state = "kos_bshorts2"
	item_state = ""
	item_color = "kos_bshorts2"
	worn_icon = 'icons/mob/infinity_work.dmi'
	icon = 'icons/obj/clothing/infinity_work.dmi'
	can_adjust = 0

/obj/item/clothing/under/womentshirt
	name = "women's T-shirt"
	desc = "Tiny, breathable white T-shirt!"
	icon_state = "women_t_shirt"
	item_state = "w_suit"
	item_color = "women_t_shirt"
	worn_icon = 'icons/mob/infinity_work.dmi'
	icon = 'icons/obj/clothing/infinity_work.dmi'
	can_adjust = 0

/obj/item/clothing/under/latex_pants
	name = "latex pants"
	desc = "Tight latex pants."
	icon_state = "latex_pants"
	worn_icon = 'icons/mob/infinity_work.dmi'
	icon = 'icons/obj/clothing/infinity_work.dmi'
	can_adjust = 0

/obj/item/clothing/under/UACcamo
	name = "trooper uniform"
	desc = "A UAC Trooper uniform. Probably not the best choice for a space station."
	worn_icon = 'icons/mob/uac/uniform.dmi'
	icon = 'icons/obj/clothing/uac/uniform.dmi'
	icon_state = "uac_uniform"
	armor = list(melee = 10, bullet = 10, laser = 10, energy = 10, bomb = 10, bio = 10, rad = 10)

/obj/item/clothing/under/shorts_denim
	name = "denim shorts"
	desc = "Jeans tight shorts."
	worn_icon = 'icons/mob/infinity_work.dmi'
	icon = 'icons/obj/clothing/infinity_work.dmi'
	icon_state = "shorts_denim"
	can_adjust = 0

/obj/item/clothing/under/dark_shorts_denim
	name = "dark denim shorts"
	desc = "Jeans tight shorts."
	worn_icon = 'icons/mob/infinity_work.dmi'
	icon = 'icons/obj/clothing/infinity_work.dmi'
	icon_state = "dark_shorts_denim"
	can_adjust = 0
